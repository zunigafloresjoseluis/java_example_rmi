public class MiClaseRemota 
 extends java.rmi.server.UnicastRemoteObject 
 implements MiInterfazRemota
{
 public MiClaseRemota() throws java.rmi.RemoteException
 {
   super(); //Llamada al constructor de la clase base (UnicastRemoteObject)
   // Código del constructor
 }
 
 public void miMetodo1() throws java.rmi.RemoteException
 {
   // Aquí ponemos el código que queramos
   System.out.println("Estoy en miMetodo1()");
 }

 public int miMetodo2() throws java.rmi.RemoteException
 {
   return 5; // Aquí ponemos el código que queramos
 }

 public void otroMetodo()
 {
   // Si definimos otro método, éste no podría llamarse
   // remotamente al no ser de la interfaz remota
 }
 
 public static void main(String[] args)
 {
   try
   {
     MiInterfazRemota mir = new MiClaseRemota();
     java.rmi.Naming.rebind("rmi://" + java.net.InetAddress.getLocalHost().getHostAddress() +
                             ":" + args[0] + "/PruebaRMI", mir);
   }
   catch (Exception e)
   {
     e.printStackTrace();
   }
 }
}